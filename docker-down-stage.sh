#!/bin/sh
set -eux
docker compose -f docker-compose.yml -f docker-compose-stage.yml down --remove-orphans
#docker rmi $(docker images --filter "dangling=true" -q --no-trunc)