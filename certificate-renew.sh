#!/bin/sh
set -eux

echo 'Debut renouvellement certificat'
docker-compose -f docker-compose.yml -f docker-compose-prod.yml run --rm certbot renew
echo 'Reload nginx server'
docker-compose exec nginx nginx -s reload

#docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
