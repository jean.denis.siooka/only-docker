#!/bin/sh
set -eux

echo 'Debut renouvellement certificat'
docker compose -f docker-compose.yml -f docker-compose-stage.yml run --rm certbot renew
echo 'Reload nginx server'
docker compose exec nginx nginx -s reload