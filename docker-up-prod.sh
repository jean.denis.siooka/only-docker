#!/bin/sh
set -eux
docker-compose -f docker-compose.yml -f docker-compose-prod.yml up --build -d
#docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
