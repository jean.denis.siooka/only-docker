#!/bin/sh
set -eux

wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && "$NVM_DIR/nvm.sh" # This loads nvm
