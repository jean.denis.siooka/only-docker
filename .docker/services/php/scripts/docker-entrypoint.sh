#!/bin/sh
set -eux
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ ! -f "composer.phar" ]; then
    install-composer
fi

#copy default .env.docker file to .env
HAS_ENV=0
if [ -f "./.env" ]; then
    echo ".env exists."
    HAS_ENV=1
else
    php -r "copy('.env.docker', '.env');"
    HAS_ENV=0
fi
#php composer.phar update --lock
php composer.phar install --prefer-dist --no-progress --no-interaction --optimize-autoloader

if [ "${HAS_ENV}" != 1 ]; then
    php artisan key:generate
    php artisan storage:link
fi

if [ ! -f "storage/oauth-private.key" ] || [! -f "storage/oauth-public.key" ]; then
    php artisan passport:keys --force
fi
#php artisan migrate
php artisan optimize:clear

# npm install
# npm run production

exec "$@"
