server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    client_max_body_size 128M;

    server_name ${NGINX_PUBLIC_HOST} ${NGINX_SIGN_HOST};
    server_tokens off;

    ssl_certificate /etc/nginx/ssl/live/${NGINX_PUBLIC_HOST}/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/${NGINX_PUBLIC_HOST}/privkey.pem;

    root /var/www/html/public;

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    # optionally disable falling back to PHP script for the asset directories;
    # nginx will return a 404 error when files are not found instead of passing the
    # request to Symfony (improves performance but Symfony's 404 page is not displayed)
    location ~ ^/(css|js|img|medias)/ {
        try_files $uri =404;
    }

    error_page 404 /page-not-found;

    location ~ ^/index\.php(/|$) {
        fastcgi_pass php:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # optionally set the value of the environment variables used in the application
        # fastcgi_param APP_ENV prod;
        # fastcgi_param APP_SECRET <app-secret-id>;
        # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        # Caveat: When PHP-FPM is hosted on a different machine from nginx
        #         $realpath_root may not resolve as you expect! In this case try using
        #         $document_root instead.
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/${NGINX_PUBLIC_HOST}_error.log;
    access_log /var/log/nginx/${NGINX_PUBLIC_HOST}_access.log;
}

server {
    listen 443 ssl http2;
    listen [::]:443;
    client_max_body_size 128M;
    server_name ${WP_PUBLIC_HOST};
    server_tokens off;
    ssl_certificate /etc/nginx/ssl/live/${WP_PUBLIC_HOST}/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/${WP_PUBLIC_HOST}/privkey.pem;
    #auth_basic "Administrator’s Area";
    #auth_basic_user_file /etc/app/only-blog/.htpassword;

    root /var/www/only-blog;

   index index.php;

   location = /favicon.ico {
           log_not_found off;
           access_log off;
   }

   location = /robots.txt {
           allow all;
           log_not_found off;
           access_log off;
   }

   location / {
           # This is cool because no php is touched for static content.
           # include the "?$args" part so non-default permalinks doesn't break when using query string
           try_files $uri $uri/ /index.php?$args;
   }

   location ~ \.php$ {
           #NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini
           include fastcgi_params;
           fastcgi_intercept_errors on;
           fastcgi_pass wpphp:9001;
           #The following parameter can be also included in fastcgi_params file
           fastcgi_param  SCRIPT_FILENAME /var/www/html$fastcgi_script_name;
   }

   location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
           expires max;
           log_not_found off;
   }

    error_log /var/log/nginx/${WP_CLUSTER_HOST}_error.log;
    access_log /var/log/nginx/${WP_CLUSTER_HOST}_access.log;
}

server {
    listen 80;
    listen [::]:80;
    server_name ${NGINX_PUBLIC_HOST} ${NGINX_SIGN_HOST} www.${NGINX_PUBLIC_HOST};
    server_tokens off;
    client_max_body_size 128M;
    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }

    location / {
        return 301 https://${NGINX_PUBLIC_HOST}$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name www.${NGINX_PUBLIC_HOST};
    server_tokens off;
    client_max_body_size 128M;
    ssl_certificate /etc/nginx/ssl/live/${NGINX_PUBLIC_HOST}/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/${NGINX_PUBLIC_HOST}/privkey.pem;

    location / {
        return 301 https://${NGINX_PUBLIC_HOST}$request_uri;
    }
}

server {
    listen 80;
    listen [::]:80;
    server_name ${WP_PUBLIC_HOST} www.${WP_PUBLIC_HOST};
    client_max_body_size 128M;
    server_tokens off;
    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }

    location / {
        return 301 https://${WP_PUBLIC_HOST}$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name www.${WP_PUBLIC_HOST};
    server_tokens off;
    client_max_body_size 128M;
    ssl_certificate /etc/nginx/ssl/live/${WP_PUBLIC_HOST}/fullchain.pem;
    ssl_certificate_key /etc/nginx/ssl/live/${WP_PUBLIC_HOST}/privkey.pem;

    location / {
        return 301 https://${WP_PUBLIC_HOST}$request_uri;
    }
}
